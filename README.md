# Project structure for web development in Kotlin

* Basic structure for any  web development (kotlin) with spring framework,deployed as **WAR** file.

* You can also deploy it with spring boot, just uncomment spring-boot-maven-plugin in the pom.xml and create your application class and main function.
```java
@SpringBootApplication
class Application
fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)
}
```
