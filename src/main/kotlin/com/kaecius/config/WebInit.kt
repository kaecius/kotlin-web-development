package com.kaecius.config

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer

open class WebInit: AbstractAnnotationConfigDispatcherServletInitializer() {
    override fun getRootConfigClasses(): Array<Class<*>> {
        return arrayOf<Class<*>>(RootConfig::class.java)
    }

    override fun getServletMappings(): Array<String> {
        return arrayOf("/")
    }

    override fun getServletConfigClasses(): Array<Class<*>> {
        return arrayOf<Class<*>>(WebConfig::class.java)
    }

}